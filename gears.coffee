# Import Section
createServer = require("http").createServer
json2xml     = require("./json2xml")
debug        = require("./debug")
connect      = require("connect")
url          = require("url")
app          = connect()


# Der Server exportiert sich selbst unter diesem Namen
gears = exports


NOT_FOUND = "Not Found\n"


# Wenn es diese URl nicht gibt wird ein 404 Fehler ausgegeben
notFound = (req, res) ->
  res.writeHead(404, {
    "Content-Type"   : "text/plain",
    "Content-Length" : NOT_FOUND.length
  })
  res.end(NOT_FOUND)




# Die Liste aller Handler die für diesen Server installiert wurden
# Es handelt sich um sogenante HashMaps (Schlüssel Wert Paare)
MyMap      = {} # Mapping füe alle Requests
MyDeb      = {} # My Debugger
MySettings = {} # Referenz auf die Server Einstellungen



# *****************************************************************************
#   Handler methods
# *****************************************************************************


# Wir geben alle Handler zurück
gears.getHandlers = () -> 
  return MyMap



# Wir speichern einen Handler oder überschreiben ihn
# Wenn Userdaten vorhanden sind kommen die mit rein
gears.setHandler = (path, Handler, Request, User, sensorRightsArray, IP) ->
  MyMap[path]             = {};
  MyMap[path]['Handler']  = Handler
  MyMap[path]['Request']  = Request
  if sensorRightsArray != undefined 
    MyMap[path]['Rights'] = sensorRightsArray

  if User != undefined
    MyMap[path]['User']      = User
    MyMap[path]['IP']        = IP;       
    MyMap[path]['LastVisit'] = new Date()
    MyDeb.High("Add Handler: " + path)



# gibt einen Handler aus der HashMap MyMap zurück solange der Schlüssel path existiert
# sonst werden Handler zurückgegeben die einen Schlüssel besitzen
gears.getHandler = (path) ->
  if path == undefined
    keys = []
    for i in MyMap
      if MyMap.hasOwnProperty(i)
        keys.push i 
    return keys
  else 
    return MyMap[path]



# löscht einen URL Handler anhand des Schlüsels path aus der HashMap MyMap
gears.removeHandler = (path) ->
  delete MyMap["/" + path]



# Nur wenn der User und die IP zum Handler passen wird dieser ausgeführt
# Wenn man sich einen Handler stielt kann man also nicht viel machen
# Weiter wird die Zeit des Handlers aktualisiert
gears.checkHandler = (User, IP) ->
  for i in MyMap
    if MyMap.hasOwnProperty(i)
      if MyMap[i].hasOwnProperty('LastVisit')
        if MyMap[i]['User'] == User && MyMap[i]['IP'] == IP
          MyMap[i]['LastVisit'] = new Date()
          return i
  return false



# Wir löschen alle Handler die älter als service_url_timeout_in_s sind 
gears.clearHandler = () ->
  MyTime = new Date().add({ seconds: - MySettings.service_url_timeout_in_s})
  for i in MyMap 
    if MyMap.hasOwnProperty(i)      
      if MyMap[i]['LastVisit'] != undefined && MyTime.isAfter(MyMap[i]['LastVisit'])
        MyDeb.High "Remove Handler: " + i
        delete MyMap[i]



runHandler = (req, res) ->
  path = url.parse(req.url).pathname
  if MyMap[path] == undefined 
    notFound(req, res)
  else
    handle = MyMap[path]['Handler']
    if MyMap[path]['User'] == undefined
      handle(req, res)
    else 
      # IP prüfen
      #if(req.connection.remoteAddress !== MyMap[path]['IP'])
      #notFound(req, res);
      MyMap[path]['LastVisit'] = new Date()
      if MyMap[path]["Rights"] != undefined
        handle(req, res, MyMap[path]["Rights"])
      else 
        handle(req, res)



res_methods = (req, res, next) ->
  res.simpleText = (code, body) ->
    res.writeHead code, {
      "Content-Type"   : "text/plain"
      "Content-Length" : body.length
    }
    res.end(body)
  

  res.simpleJSON = (code, obj) ->
    body = new Buffer(JSON.stringify(obj))
    res.writeHead code, {
      "Content-Type"   : "text/json"
      "Content-Length" : body.length
    }
    res.end(body)

  
  res.simpleXML = (code, obj) ->
    body = new Buffer(json2xml({data : obj}, { header: true }))
    res.writeHead code, {
      "Content-Type"   : "text/xml"
      "Content-Length" : body.length
    }
    res.end(body)    
  
  #Call Next Step
  next()  



# Wir konfigurieren unseren Server indem wir ihm das app Objekt mitgeben
app.use(connect.bodyParser({strict: false}))
app.use(res_methods)
app.use(runHandler)
app.use(connect.query())

server = createServer(app)


# Aufraeumen starten
# Wir sehen aller 30 Sekunden nach ob Handler entfernt werden müssen
setInterval(gears.clearHandler, 30000)



# Startet den Server so das er lauscht
gears.listen = (port, host, settings) ->
  server.listen(port, host)
  MySettings = settings;
  MyDeb      = new debug(settings.debug_level)
  MyDeb.High 'data-node.js-service is up and running.'
  MyDeb.High '|-> IP: '   + host
  MyDeb.High '|-> Port: ' + port  



# schließt den Server wieder
gears.close =  () ->
  server.close()



gears.getClientIp = (req) ->
  ipAddress
  # Amazon EC2 / Heroku workaround to get real client IP
  #var forwardedIpsStr = req.headers['x-forwarded-for']; 
  if (req.headers['x-forwarded-for'] != undefined) 
    # 'x-forwarded-for' header may return multiple IP addresses in
    # the format: "client IP, proxy 1 IP, proxy 2 IP" so take the
    # the first one
    forwardedIps = req.headers['x-forwarded-for'].split(',')
    ipAddress    = forwardedIps[0]
  
  if !ipAddress 
    # Ensure getting client IP address still works in
    # development environment
    ipAddress = req.connection.remoteAddress
  
  return ipAddress