# Loading of basic settings
# |-> Every client has his own config file 
Settings     = require './node-service-settings.json'


debug        = require "./debug"
gears        = require "./gears"
crypto       = require "crypto"
utils        = require "date-utils"
sys          = require "util"
url          = require "url"
mysql        = require "mysql"
moment       = require "moment"
async        = require "async"


# initiating the debugger
deb = new debug(Settings.debug_level)


#HTTP or HTTPS Login@Meta-DB?
if Settings.https_login == true
  request_methode = require("https")
else
  request_methode = require("http")


#Check client should run in multi database mode
if Settings.global_meta_database?
  multi = true
  Settings.global_meta_database.db_id = 'multi' 
  db_settings = Settings.global_meta_database
  deb.High 'Starting client in multi database mode !'
else
  multi = false
  db_settings = Settings.local_raw_database
  deb.High 'Starting client in single database mode for db_id: ' + db_settings.db_id
  
  
# Set standard MySQL-Connection
pools = []
pools[0] = mysql.createPool {
  host            : db_settings.mysql_url,
  port            : db_settings.mysql_port,
  user            : db_settings.username,
  password        : db_settings.password,
  database        : db_settings.database,
  connectionLimit : 2
}

# array for locking during mysql setup
lock = []
wait = []

# We catch all exceptions
process.on 'uncaughtException', (err) ->
  deb.High 'Caught exception: ' + err.stack  #err.stack stack für ausführlichere Fehlermeldungen


# Starting WebServer
# This Port Seems to be ignored here
ip   = Settings.node_service_IP    || "127.0.0.1"
port = Settings.node_service_port  || 9000

gears.listen(port, ip, Settings)


# *****************************************************************************
#   CallBacks for Every Request
# *****************************************************************************

# Set handler  for auth and login
gears.setHandler('/', (req, res) ->

  header   = req.headers['authorization'] || ''        # get the header
  token    = header.split(/\s+/).pop() || ''           # and the encoded auth token
  auth     = new Buffer(token, 'base64').toString()    # convert from base64
  parts    = auth.split(/:/)                           # split on colon
  username = parts[0] 
  password = parts[1]  

  if (username && password) 
    Authenticate(username, password, req, res, login) 
  else  
    SendResponse(res, {"globalErrors" : ["You have to deliver your username and password!"], "sensorErrors" : []}, 401) 
   
, 'GET')


# Set handler for logout
gears.setHandler('/logout', (req, res) ->
    handler = url.parse(req.url, true).query['handler']
    logout(handler,req,res)    
, 'GET')



# Check if testmode is active
if Settings.test_mode == true 
  #function that shows all existing handlers
  gears.setHandler("/handlers", (req, res) ->
    deb.Medium('Got /handlers Request')    
            
    handlers = gears.getHandlers()
            
    Answer = {
      count    : Object.keys(handlers).length,
      handlers : handlers        
    }
            
    res.simpleJSON(200, Answer)
    deb.Medium('|-> Request Answered.')
            
  , 'GET');    


  # function shows some informations
  gears.setHandler("/info", (req, res) ->
    deb.Medium('Got /info Request') 
        
    Answer = {
      name                     : "node.js service for data project",
      authors                  : "Jens Haupt, Karl Hoffmann, Jacob Horak", 
      mysql_url                : db_settings.mysql_url,
      mysql_port               : db_settings.mysql_port,
      mysql_database_id        : db_settings.db_id, 
      mysql_database           : db_settings.database,
      node_service_port        : Settings.node_service_port,
      service_url_timeout_in_s : Settings.service_url_timeout_in_s, 
      IP                       : gears.getClientIp(req),
      debug_level              : Settings.debug_level,
      test_mode                : Settings.test_mode
    }    
        
    res.simpleJSON(200, Answer)    
    deb.Medium('|-> Request Answered.')        
        
  ,'GET')


# function to process the delivered data
process_data = (req, res, sensorRightsArray) ->    

  globalErrors = [] 
  sensorErrors = []

  # check for correct Content-Type
  if (req.body.constructor != Array) && (!req.body[0])
    globalErrors.push "No correct Content-Type!"
    SendResponse(res, {"globalErrors" : globalErrors, "sensorErrors" : sensorErrors}, 202)
  else 
    # mapping en index to each sensor
    for j in [0...req.body.length]
      req.body[j].index      = j
    
    async.each req.body , (sensor_data, operationIsFinishedCallback) ->
      
      # This finishes The Async Parallel chain
      finishThis = (myError) ->
        if myError.errors.length > 0
          sensorErrors.push myError       
        operationIsFinishedCallback(null)

      sensor_id             = undefined 

      # Defining an Error Object for this Sensor
      myError               = {}                 # Empty Object
      myError.index         = sensor_data.index  # Error belongs to this SensorData
      myError.errors        = []                 # Adding an Array of all Errors to this Sensor
      

      #Vergleich der Sensoren mit sensorRightsArray
      if ((sensor_data.local_sensor_id == undefined) && (sensor_data.global_sensor_id == undefined) && (sensor_data.uuid == undefined)) 
        myError.errors.push { id_error : "global_sensor_id or local_sensor_id or uuid has to be delivered" }
        empty  = true 


      # we prefer local Ids
      # if we deliver local stuff we have to deliver database ID
      # its to ensure consistency
      if sensor_data.local_sensor_id != undefined
        if sensor_data.db_id  == undefined     
          myError.errors.push { db_id_error : "If you use local_sensor_id you have to deliver db_id!"}
        else  
          for i in [0...sensorRightsArray.length] 
            if sensorRightsArray[i]["local_sensor_id"] == sensor_data.local_sensor_id.toString()
              sensor_id = sensor_data.local_sensor_id
              db_id = sensorRightsArray[i]["db_id"]
              break
      else  
        # If no local_sensor_id is given, try to get it with the help of global_sensor_id or uuid
        for i in [0...sensorRightsArray.length]
          
          if ((sensor_data.global_sensor_id != undefined) && (sensorRightsArray[i]["global_sensor_id"] == sensor_data.global_sensor_id.toString()))
            sensor_id = sensorRightsArray[i]["local_sensor_id"]
            db_id = sensorRightsArray[i]["db_id"]
            break
      
          if ((sensor_data.uuid != undefined) && (sensor_data.uuid != "") && (sensorRightsArray[i]["uuid"] == sensor_data.uuid))
            sensor_id = sensorRightsArray[i]["local_sensor_id"]      
            db_id = sensorRightsArray[i]["db_id"]
            break

      if sensor_id == undefined 
        if !empty
          myError.errors.push { rights_error : "You are not allowed to deliver sensor data!" }
       
        finishThis(myError) 

      else 
        #console.log sensor_data
        if sensor_data.value != null 
          
          MySQLQuery = BuildMySQLQuerry(sensor_id, sensor_data.value, sensor_data.utc_time) 
          
          # This is called in an Async way
          getDbConnection db_id, (err, connection ) -> 
            
            if !err
              # Use the connection
              success = connection.query MySQLQuery, (err, result) ->             

                if !err

                  connection.release()      
                  finishThis(myError)

                else 
                     # catch DB errors
                  switch err.code
                    when "ER_NO_REFERENCED_ROW_"
                      myError.errors.push { no_referenced_row_error : "sensor does not exist in local_sensor_db" }

                    when "ER_DUP_ENTRY"
                      myError.errors.push { duplicate_entry_error : "cannot write duplicate entries" }

                    else 
                      # add other errors to myError Stack            
                      myError.errors.push { my_sql_error : { code : err.code, stack : err.stack } }

                  connection.release()
                  finishThis(myError)          

            else
              myError.errors.push { my_sql_error : { code : err.code, stack : err.stack } }
              globalErrors.push   { my_sql_error : { code : err.code, stack : err.stack } }
            
              if connection?
                connection.release()
              finishThis(myError)

        else
          myError.errors.push { value_error : "A value has to be delivered!" }
          finishThis(myError)

    , (anError) ->
      if sensorErrors.length > 0   
        SendResponse(res, {"globalErrors" : globalErrors, "sensorErrors" : sensorErrors}, 202)
      else
        SendResponse(res, {"globalErrors" : globalErrors, "sensorErrors" : sensorErrors}, 200)   


# Set handler for remove old handlers
gears.setHandler("/clear", (req, res) ->
    gears.clearHandler()
    res.simpleText(200, "Job done!")
,'GET')



# *****************************************************************************
#   Some Helper Methods
# *****************************************************************************
# function to get size of object
Object.size = (obj) ->
  size = 0   
  for key in obj  
    if obj.hasOwnProperty(key) 
      size++
  return size


# function .push() for objects
PushObject = (obj, value, j) ->
  size = Object.size(obj[j])
  obj[j][size] = value
  return obj[j]

  
# function isEmty for objects
isEmpty = (obj) ->
  for prop in obj 
    if obj.hasOwnProperty(prop)
      return false 
  return true 


SendResponse = (res, Errors, code) ->
  if Errors.globalErrors.length + Errors.sensorErrors.length > 0 
    deb.High "Error: " + JSON.stringify(Errors)
  
  if code == undefined
    code = 200
  
  res.simpleJSON(code, Errors )
  Errors = ""        



# Building SQL Query to insert value
BuildMySQLQuerry = (id, value, time) ->
  MyTime    
    
  if time == undefined   
    MyTime = new Date()  # this delivers always UTC
  else 
    MyTime = new Date(Date.parse(time + ' GMT'))
           
  return "INSERT INTO `rawdata` (`local_sensor_id`, `time`, `split_second`, `value`) VALUES ( '" + id + "', '" + MyTime.toUTCFormat("YYYY-MM-DD HH24:MI:SS") + "', '" + MyTime.getMilliseconds() + "' , '" + value + "' );"      


# *****************************************************************************
# These function checks if this user is allowed to add data rawdata table
# Maintainer: Karl
Authenticate = (username, password, req, res, next) ->
  options = {
    host               : Settings.login_url                    
    method             : 'GET'
    path               : '/site/GetWriteAccess?LoginForm%5Busername%5D=' + username + '&LoginForm%5Bpassword%5D=' + password + '&db_id=' + db_settings.db_id
    rejectUnauthorized : false 
  }

  callback = (response) ->
    data = '';
    
    response.on 'data', (chunk) ->
      data += chunk
    
    response.on 'end', () ->
      if response.statusCode == 200
        sensorRightsArray = JSON.parse(data);
        next( username, sensorRightsArray, req, res )
      else 
        SendResponse(res, {"globalErrors" : ["Wrong username or password!"], "sensorErrors" : []}, 401)
      
    response.on 'error', (err) -> 
      deb.High 'Error requesting Access Rights: ' + err.message

  request_methode.get(options, callback).end()

# This creates a new Path for Data Handling
BuildNewDataPath = () ->
  MyTime = new Date()
  # Our Encoding Class
  return crypto.createHash('md5').update(MyTime.toISOString()).digest("hex") 


# function to login and set individual handler
login = ( username, sensorRightsArray, req, res) ->
  # Get current IP Adress 
  current_ip = gears.getClientIp(req)
                
  DataURL    = "/" + BuildNewDataPath()
  gears.setHandler(DataURL, process_data, 'POST', username, sensorRightsArray, current_ip)
    
  # @testmode installing handler /data to deliver data
  if Settings.test_mode == true 
    gears.setHandler("/data", process_data,'POST', username, sensorRightsArray)
    

  res.simpleJSON 200, {
    db_id        : Settings.db_id,
    url          : DataURL,
    username     : username,
    status       : "logged in",
    sensorRights : sensorRightsArray
  }

# function to logout
logout = (handler, req, res) ->
  # If User is logged in remove URL 
  gears.removeHandler(handler)
                
  res.simpleJSON 200, {
    url      : handler,
    status   : "logged out",
  }
  
# function to get multiply DB access 
getDbConnection = (db_id, callback) ->
  if pools[db_id] == undefined 
    if lock[db_id] == true 
      deb.Low "|-> Status: getDbConnection is locked for DB-ID: " + db_id
      wait[db_id].push callback 
    else
      lock[db_id] = true 
      wait[db_id] = []
      sql_query = "SELECT `db_name`, `url`, `port`, `username`, `password` FROM `database` WHERE `id`=?"
      pools[0].query sql_query, [db_id], (err, row) ->
        if !err    
          if row.length == 1
            pools[db_id] = mysql.createPool {
              host            : row[0]["url"],
              port            : row[0]["port"],
              user            : row[0]["username"],
              database        : row[0]["db_name"],
              password        : row[0]["password"],
              connectionLimit : 3
            }
            lock[db_id] = false 
            deb.Low "|-> Status: Setup DB-Connection " + db_id
            pools[db_id].getConnection(callback)
            resolveWait db_id
          else
            err = {
              stack : "No connection for db_id: " + db_id + " available!",
            }
            lock[db_id] = false 
            resolveWait db_id
            callback(err, null) 
        else
          lock[db_id] = false 
          resolveWait db_id
          callback(err, null) 
  else 
    pools[db_id].getConnection(callback)
 
resolveWait = (db_id) ->
  if lock[db_id] == true 
    deb.High "|-> Error: Tried to resolve waiting DbConnections!"
  else
    for fn in wait[db_id]
      getDbConnection db_id, fn
    deb.Low "|-> Status: All DbConnections resolved!"
